package org.hackdaycity.photostalker;

import org.hackdaycity.photostalker.model.Building;
import org.hackdaycity.photostalker.model.Response;
import org.hackdaycity.photostalker.model.Coord;
import org.hackdaycity.photostalker.model.Locator;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by anton on 4/26/14.
 */
public class LocatorTest {
     private Locator locator = new Locator();


    public LocatorTest() throws FileNotFoundException {
        //locator.setBuildCoords(sampleBuilding());
        locator.setBuildCoords(Main.realBuidingData());
    }

    @Test
    public void getBuildingsAroundMe() {
        Coord myCoord = new Coord(30.361080f, 59.922399f);
        List<Response> buildingsAroundMe = locator.getBuildingsAroundMe(myCoord);
        for (Response response : buildingsAroundMe) {
            System.out.println("Result: " + response.toString());
        }
    }

    private List<Building> sampleBuilding() {
        Building b1 = new Building(1, Arrays.asList(
                new Coord(30.360794f, 59.922188f),
                new Coord(30.360823f, 59.922058f),
                new Coord(30.361080f, 59.922005f),
                new Coord(30.361298f, 59.922085f),
                new Coord(30.361254f, 59.922222f),
                new Coord(30.361006f, 59.922272f),
                new Coord(30.360794f, 59.922188f))
        );

        Building b2 = new Building(2, Arrays.asList(
                new Coord(30.348909f, 59.928150f),
                new Coord(30.348768f, 59.928001f),
                new Coord(30.348808f, 59.928005f),
                new Coord(30.348989f, 59.927979f),
                new Coord(30.349045f, 59.928143f),
                new Coord(30.348909f, 59.928150f)));

        Building b3 = new Building(3, Arrays.asList(
                new Coord(30.360223f, 59.921915f),
                new Coord(30.361859f, 59.921678f),
                new Coord(30.361634f, 59.921338f),
                new Coord(30.360012f, 59.921591f)
                ));
        return Arrays.asList(b1, b2, b3);
    }
}
