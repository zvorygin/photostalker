package org.hackdaycity.photostalker.model;

/**
 * Created by anton on 4/26/14.
 */
public class Response {
    public final Building building;
    public double minAngle, maxAngle;

    public Response(Building building, double minAngle, double maxAngle) {
        this.building = building;
        this.minAngle = minAngle;
        this.maxAngle = maxAngle;
    }


    @Override
    public String toString() {
        return "Response{" +
                "id='" + building.getId() + '\'' +
                ", name=" + building.getName() +
                ", minAngle=" + minAngle +
                ", maxAngle=" + maxAngle +
                '}';
    }
}
