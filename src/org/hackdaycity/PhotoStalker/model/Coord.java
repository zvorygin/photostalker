package org.hackdaycity.photostalker.model;

import java.io.Serializable;

/**
 * Created by anton on 4/26/14.
 */
public class Coord implements Serializable{
    double y, x;

    public Coord(double x, double y) {
        this.y = y;
        this.x = x;
    }

    @Override
    public String toString() {
        return x + "," + y;
    }


}
