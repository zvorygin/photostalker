package org.hackdaycity.photostalker.model;

import java.util.*;

/**
 * Created by anton on 4/26/14.
 */
public class Locator {
    private List<Building> buildings;

    public void setBuildCoords(List<Building> buildCoords) {
        this.buildings = buildCoords;
    }

    public List<Response> getBuildingsAroundMe(Coord myCoords) {
        sortByCurrentDistance(myCoords);

        List<Response> result = new LinkedList<Response>();
        for (Building building : buildings) {
            List<Response> buildingAngles = viewingAnglesForBuilding(myCoords, building);
            //System.out.println(buildingAngles);
            //System.out.println();
            for (Response toAdd : buildingAngles) {
                include0(result, toAdd, building);

            }
        }
        return result;
    }

    private void include0(List<Response> result, Response toAdd, Building building) {
        ListIterator<Response> resultIterator = result.listIterator();
        Response currentResponse;
        double anglePointer = toAdd.minAngle;
        while (true) {
            if (!resultIterator.hasNext()) {
                currentResponse = null;
                break;
            }
            currentResponse = resultIterator.next();
            if (currentResponse.minAngle < anglePointer && currentResponse.maxAngle > anglePointer)
                anglePointer = currentResponse.maxAngle;
            if (currentResponse.minAngle >= toAdd.minAngle)
                break;
        }


        if (currentResponse == null || anglePointer >= toAdd.maxAngle) {
            if (anglePointer < toAdd.maxAngle)
                result.add(new Response(building, anglePointer, toAdd.maxAngle));
                 return;
        }

        while (true) {
            if (anglePointer < Math.min(currentResponse.minAngle, toAdd.maxAngle)) {
                Response r1 = resultIterator.previous();
                Response add = new Response(building, anglePointer, Math.min(currentResponse.minAngle, toAdd.maxAngle));
                resultIterator.add(add);
                Response r2 = resultIterator.next();
            }
            anglePointer = Math.min(currentResponse.maxAngle, toAdd.maxAngle);
            if (anglePointer >= toAdd.maxAngle)
                break;

            if (!resultIterator.hasNext()) {
                currentResponse = null;
                break;
            }
            currentResponse = resultIterator.next();
        }

        if (anglePointer < toAdd.maxAngle) {
            resultIterator.add(new Response(building, anglePointer, toAdd.maxAngle));
        }

            /*
            resultIterator = result.listIterator();
            currentResponse = resultIterator.next();
            while (resultIterator.hasNext()) {
                Response next = resultIterator.next();
                if (next.minAngle < currentResponse.maxAngle) {
                    System.out.println("hell");

                }
                currentResponse = next;
            }
            */
    }

    private void sortByCurrentDistance(Coord myCoords) {
        final Map<Integer, Double> distance = new HashMap<>();
        for (Building building : buildings) {
            distance.put(building.getId(), building.calculateDistance(myCoords));
        }

        Collections.sort(buildings, new Comparator<Building>() {
            @Override
            public int compare(Building building, Building building2) {
                return Double.compare(distance.get(building.getId()), distance.get(building2.getId()));
            }
        });

    }

    private List<Response> viewingAnglesForBuilding(Coord myCoords, Building building) {
        double min = 360;
        double max = 0;
        double maxLess180 = 0;
        double minMore180 = 360;
        for (Coord buildCoord : building.getCoords()) {
            double thetaRadians = Math.atan2(buildCoord.y - myCoords.y, buildCoord.x - myCoords.x);
            double thetaDegrees = thetaRadians * 180 / Math.PI;
            if(thetaDegrees < 0) thetaDegrees += 360;
            if(thetaDegrees > max) max = thetaDegrees;
            if(thetaDegrees < min) min = thetaDegrees;
            if(thetaDegrees < 180 && thetaDegrees > maxLess180) maxLess180 = thetaDegrees;
            if(thetaDegrees > 180 && thetaDegrees < minMore180) minMore180 = thetaDegrees;
            //System.out.println("Theta : " + thetaDegrees);
        }
        if(min + 180 < max) {
            return Arrays.asList(new Response(building, 0, maxLess180), new Response(building, minMore180, 360));
        } else {
            return Arrays.asList(new Response(building, min, max));
        }
    }
}