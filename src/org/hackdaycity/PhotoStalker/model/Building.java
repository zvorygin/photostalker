package org.hackdaycity.photostalker.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by anton on 4/26/14.
 */
public class Building implements Serializable{
    int id;
    String name;
    List<Coord> coords;
    public boolean isInteresting;

    public String date;
    public String address;
    public String arch;

    public Building(int id, String name, List<Coord> coords) {
        this.id = id;
        this.name = name;
        this.coords = coords;

    }

    public Building(int id, List<Coord> coords) {
        this.id = id;
        this.coords = coords;
    }

    public int getId() {
        return id;
    }

    public Coord getCenterCoord() {
        return coords.get(0);
    }

    public List<Coord> getCoords() {
        return coords;
    }


    public void setCoords(List<Coord> coords) {
        this.coords = coords;
    }

    public double calculateDistance(Coord myCoord) {
        Coord centerCoord = getCenterCoord();
        return Math.hypot(centerCoord.x - myCoord.x, centerCoord.y - myCoord.y);
    }

    boolean isInteresting() {
        return true || isInteresting;
    }

    @Override
    public String toString() {
        return "Building{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isInteresting=" + isInteresting +
                ", date='" + date + '\'' +
                ", address='" + address + '\'' +
                ", arch='" + arch + '\'' +
                '}';
    }

    public String getName() {
        return name;//.length() > 20 ? "..." + name.substring(name.length() - 15, name.length())  : name;
    }
}