package org.hackdaycity.photostalker;

import android.content.ClipData;
import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import org.hackdaycity.photostalker.model.Coord;
import org.hackdaycity.photostalker.model.Locator;
import org.hackdaycity.photostalker.model.Response;

import java.util.*;

/**
 * Created by zvorygin on 4/26/14.
 */
public class OverlayView extends View {
    private Paint mTextPaint;
    private static final String TAG = "OverlayView";

    private int smoothingBufferDepth = 7;
    private double[] smoothingBuffer = new double[1 << smoothingBufferDepth];
    private double[] smoothingBufferTemp = new double[1 << smoothingBufferDepth];
    private int currentBufferIdx = 0;
    private double smoothAzimuth = 0;

    private double horizontalAngle;
    private Locator locator;
    private Location location;
    private Coord myCoords;
    private Paint mGreyPaint;
    private Paint mCirclePaint;
    private boolean enabled;
    private TextView textView;

    void setTextView(TextView tv) {
        this.textView = tv;
    }

    public OverlayView(Context context) {
        super(context);
        init();
    }

    public OverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setAzimuth(double azimuth) {
        smoothingBuffer[currentBufferIdx] = azimuth;

        int index = currentBufferIdx;
        for (int i = 0; i < smoothingBuffer.length; i++) {
            smoothingBufferTemp[i] = smoothingBuffer[index];
            index = (index + smoothingBuffer.length - 1) % smoothingBuffer.length;
        }
        for (int j = 0; j < smoothingBufferDepth; j++) {
            for (int i = 0; i < 1 << (smoothingBufferDepth - j - 1); i++) {
                smoothingBufferTemp[i] = getMiddle(smoothingBufferTemp[2 * i], smoothingBufferTemp[2 * i + 1]);
            }
        }
        smoothAzimuth = smoothingBufferTemp[0];

        currentBufferIdx = (currentBufferIdx + 1) % smoothingBuffer.length;
        invalidate();
    }

    private double getMiddle(double a, double b) {
        double max = Math.max(a, b);
        double min = Math.min(a, b);
        if (max - min < 180) {
            return (min + max) / 2;
        } else {
            double r = (min + max) / 2;
            return (r + 180) % 360;
        }
    }

    public void setHorizontalAngle(double horizontalAngle) {
        this.horizontalAngle = horizontalAngle;
    }

    private void init() {
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        mTextPaint.setColor(getResources().getColor(android.R.color.holo_blue_light));
        mTextPaint.setTextSize(30);
        mTextPaint.setTextAlign(Paint.Align.LEFT);

        mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCirclePaint.setColor(mTextPaint.getColor());

        mGreyPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mGreyPaint.setColor(Color.argb(256 - 32, 0, 0, 0));
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!this.enabled) {
//            return;
        }

        float accuracy = 0;

        if (this.location == null || !this.location.hasAccuracy() || (accuracy = this.location.getAccuracy()) > 100) {
            String message = ((accuracy == 0) ? "Determining location" : String.format("Waiting for more precise location (%.1f m).", accuracy));
            canvas.drawText(message, 100, 100, mTextPaint);
            return;
        }

        //canvas.drawText(String.format("GPS accuracy (%.1f m).", accuracy), 0, mTextPaint.getTextSize(), mTextPaint);

        int width = canvas.getWidth();
        int height = canvas.getHeight();

        List<Response> visible = getBuildingsAround();

        int offset = getViewportCoordinate(90, width);


        int size = visible.size();

        List<Response> near = new LinkedList<>();
        List<Response> far = new LinkedList<>();

        for (int i = 0; i < visible.size(); i++) {
            Response response = visible.get(i);
            if (i < size / 2) {
                near.add(response);
            } else {
                far.add(response);
            }
        }

        drawItems(canvas, width, 0, far, height / 3);
        drawItems(canvas, width, height - 75, near, height * 2 / 3);

        if ((offset > 0) && offset < width) {
            canvas.drawLine(offset, 0, offset, height, mTextPaint);
        }
    }

    private List<Response> getBuildingsAround() {
        List<Response> buildingsAroundMe = locator.getBuildingsAroundMe(myCoords);


        List<Response> visible = new LinkedList<Response>();
        for (Response response : buildingsAroundMe) {
            if (!response.building.isInteresting)
                continue;
            double from = smoothAzimuth - horizontalAngle / 2;
            double to = smoothAzimuth + horizontalAngle / 2;
            if (from < 0) {
                from += 360;
                to += 360;
            }

            if (to <= 360) {
                addVisible(visible, response, from, to);
            } else {
                addVisible(visible, response, from, 360);
                addVisible(visible, response, 0, to - 360);
            }
        }

        visible = filterBuildings(visible);

        Collections.sort(visible, new Comparator<Response>() {
            @Override
            public int compare(Response response, Response response2) {
                return Double.compare(response.building.calculateDistance(myCoords),
                        response2.building.calculateDistance(myCoords));
            }
        });
        return visible;
    }

    private List<Response> filterBuildings(List<Response> buildingsAroundMe) {
        List<Response> result = new ArrayList<>();

        Response current = null;

        Map<Integer, Response> buildingId2Response = new HashMap<>();

        for (Response response : buildingsAroundMe) {
            if (current == null) {
                current = response;
                continue;
            }

            if (current.building == response.building) {
                current = new Response(current.building, Math.min(current.minAngle, response.minAngle), Math.max(response.maxAngle, current.maxAngle));
            } else {
                replaceIfNeeded(current, buildingId2Response);
                current = response;
            }
        }

        if (current != null) {
            replaceIfNeeded(current, buildingId2Response);
        }

        result.addAll(buildingId2Response.values());

        return result;
    }

    private void replaceIfNeeded(Response current, Map<Integer, Response> buildingId2Response) {
        if (!buildingId2Response.containsKey(current.building.getId())) {
            buildingId2Response.put(current.building.getId(), current);
        } else {
            Response existing = buildingId2Response.get(current.building.getId());

            if ((existing.maxAngle - existing.minAngle) < (current.maxAngle - current.minAngle)) {
                buildingId2Response.put(current.building.getId(), current);
            }
        }
    }

    private void drawItems(Canvas canvas, int width, int top, List<Response> far, float markerHeight) {
        canvas.drawRect(0, top, width, top + 75, mGreyPaint);
        int textWidth = 0;
        for (Response response : far) {
            textWidth += mTextPaint.measureText(response.building.getName());
        }

        float textSize = mTextPaint.getTextSize();

        if (textWidth > width)
            mTextPaint.setTextSize(textSize * width / textWidth);

        int textOffset = 0;

        for (Response response : far) {
            int left = getViewportCoordinate(response.minAngle, width);
            int right = getViewportCoordinate(response.maxAngle, width);

            if (left > right) {
                // !!!!!
                int temp = left;
                left = right;
                right = temp;
            }


            canvas.drawRect(left + 3, markerHeight, right - 3, markerHeight + 10, mTextPaint);

            drawMarker(canvas, (right + left) / 2, markerHeight + (response.building.getName().hashCode() % 100) - 50);

            canvas.drawText(response.building.getName(), textOffset, top + mTextPaint.getTextSize() + 10, mTextPaint);

            float labelWidth = mTextPaint.measureText(response.building.getName());

            canvas.drawLine((right + left) / 2, markerHeight + (response.building.getName().hashCode() % 100) - 50, textOffset + labelWidth / 2, top + mTextPaint.getTextSize() + 10, mTextPaint);

            textOffset += labelWidth;
        }


        mTextPaint.setTextSize(textSize);
    }

    private void drawMarker(Canvas canvas, float x, float y) {
        mCirclePaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(x, y, 15, mCirclePaint);

        mCirclePaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(x, y, 10, mCirclePaint);
    }

    private int getViewportCoordinate(double angle, double width) {
        return (int) (width * (0.5 - Math.tan((angle - smoothAzimuth) / 2.0 / 360 * 2 * Math.PI) / Math.tan(horizontalAngle / 2.0 / 360 * 2 * Math.PI)));
    }

    private void addVisible(List<Response> visible, Response response, double from, double to) {
        double min = Math.max(from, response.minAngle);
        double max = Math.min(to, response.maxAngle);
        if (min < max) {
            visible.add(new Response(response.building, min, max));
        }
    }

    public void setLocator(Locator locator) {
        this.locator = locator;
    }

    public void setLocation(Location location) {
        this.location = location;
        myCoords = new Coord(this.location.getLongitude(), this.location.getLatitude());
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();

        int width = getWidth();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                List<Response> buildingsAround = getBuildingsAround();

                for (Response response : buildingsAround) {
                    int left = getViewportCoordinate(response.minAngle, width);
                    int right = getViewportCoordinate(response.maxAngle, width);

                    Log.d("blah", String.format("Comparing %.2f against [%d-%df]", eventX, left, right));

                    if (((eventX > left && eventX < right) || (eventX < left && eventX > right)) && textView != null) {
                        textView.setText(String.format("Название - %s\n Год постройки %s\n Адрес - %s\n Архитектор - %s", response.building.getName(), response.building.date, response.building.address, response.building.arch));
                        textView.setVisibility(VISIBLE);

                        disable();
                        return true;
                    }
                }

                return false;
            default:
                return false;
        }
    }

    public void enable() {
        this.enabled = true;
    }

    public void disable() {
        this.enabled = false;
    }
}
