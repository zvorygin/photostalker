package org.hackdaycity.photostalker;

import org.hackday.photostalker.R;
import org.hackdaycity.photostalker.model.Building;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws Exception {
        List<String> l = new LinkedList<>();
        l.add("1");
        ListIterator<String> stringListIterator = l.listIterator();
        stringListIterator.next();
        stringListIterator.previous();
        stringListIterator.add("0");
        System.out.println(l);
    }

    public static List<Building> realBuidingData() throws FileNotFoundException {
        return sampleBuildings(new FileInputStream("res/raw/data.bin"));
    }

    private static void addBefore(ListIterator<String> stringListIterator) {
        System.out.println(stringListIterator.previous());
        stringListIterator.add("5");
        System.out.println(stringListIterator.next());
    }

    private static List<Building> sampleBuildings(InputStream input) throws RuntimeException {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(input)) {
            return (List<Building>) objectInputStream.readObject();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
